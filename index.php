<?php
require __DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'data.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <!-- YOUR FAVICO -->
    <link rel="icon" href="/web/img/favicon.jpg">

    <title>Сота</title>

    <!-- Latest compiled and minified CSS -->
    <!-- BOOTSTRAP CDN CSS FILE LINK -->
    <link rel="stylesheet" href="/web/css/bootstrap.min.css">
    <link rel="stylesheet" href="/web/css/bootstrap-theme.min.css">
    <!-- Custom styles for this template -->
    <style>
        body {
            padding-top: 50px;
        }

        img.center {
            display: block;
            margin: 0 auto;
        }

        .my-class {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            align-items: flex-start;
        }

        table {
            width: 30% !important;
        }
    </style>
</head>

<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Мягкая мебель "СОтА"</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav"></ul>
        </div>
    </div>
</nav>

<div class="container">
    <h1 class="text-center">Список товаров</h1>

    <div class="my-class">
        <?php /** @var \app\models\Product $product */
        foreach ($products as $product): ?>
            <table class="table table-bordered">
                <tr>
                    <td class="text-center" colspan="2">
                        <img src="<?= $product->getImg(); ?>" class="img-thumbnail center">
                    </td>
                </tr>
                <tr>
                    <td class="text-center" colspan="2">
                        <?= $product->getName(); ?>
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        <?= $product->getPrice(); ?> $
                    </td>
                    <td class="text-center">
                        Добавлено <?= $product->getCreatedAt(); ?>
                    </td>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<!-- BOOTSTRAP CDN JS FILE LINK -->
<script src="/web/js/bootstrap.min.js"></script>
</body>
</html>
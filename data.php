<?php

use app\models\Product;

$data = [
    [
        'img' => '/web/img/goods/sota-1.jpg',
        'name' => 'Сота-1',
        'price' => '393',
        'created_at' => '2017-12-03',
    ],
    [
        'img' => '/web/img/goods/sota-6.jpg',
        'name' => 'Сота-6',
        'price' => '126',
        'created_at' => '2017-12-04',
    ],
    [
        'img' => '/web/img/goods/sota-6x-9.jpg',
        'name' => 'Сота-6х/9',
        'price' => '166',
        'created_at' => '2017-12-05',
    ],
    [
        'img' => '/web/img/goods/sota-8.jpg',
        'name' => 'Сота-8',
        'price' => '366',
        'created_at' => '2017-12-07',
    ],
    [
        'img' => '/web/img/goods/sota-13.jpg',
        'name' => 'Сота-13',
        'price' => '666',
        'created_at' => '2017-12-08',
    ],
    [
        'img' => '/web/img/goods/sota-56.jpg',
        'name' => 'Сота-56',
        'price' => '366',
        'created_at' => '2017-12-09',
    ],
    [
        'img' => '/web/img/goods/sota-city.jpg',
        'name' => 'Сота СИТИ',
        'price' => '233',
        'created_at' => '2017-12-10',
    ],
    [
        'img' => '/web/img/goods/tumba.jpg',
        'name' => 'Тумбочка для обуви',
        'price' => '48',
        'created_at' => '2017-12-10',
    ],
];
$products = [];
foreach ($data as $item) {
    $product = new Product();
    $product->setImg($item['img']);
    $product->setName($item['name']);
    $product->setPrice($item['price']);
    $product->setCreatedAt($item['created_at']);

    $products[] = $product;
};